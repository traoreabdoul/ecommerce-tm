from django.db import models
from django.contrib.auth.models import User

# Create your models here.
# Create your models here.
class User(models.Model): 
    nom = models.CharField (max_length = 50) 
    prenom = models.CharField(max_length=50)
    contact = models.CharField (max_length = 30,) 
    email = models.EmailField (blank = True) 
    mot_passe = models.CharField(blank=True,max_length=8) 
    confirm_passe= models.CharField(blank=True,max_length=8)
    
    def __str__(self):
        return self.nom

class Produit(models.Model):
    nom= models.CharField(max_length=20)
    genre=models.ForeignKey('Genre',on_delete=models.CASCADE)
    montant=models.IntegerField()
    description = models.TextField(max_length=100)
    quantite=models.IntegerField()
    image=models.ImageField()

    def __str__(self):
        return self.nom

class Genre(models.Model):
    nom=models.CharField(max_length=30)
    categorie=models.ForeignKey('Categorie',on_delete=models.CASCADE)
    def __str__(self):
        return self.nom


class Categorie(models.Model):
    nom = models.CharField (max_length =30)
    def __str__(self):
        return self.nom


class Livreur(models.Model):
    nom=models.CharField(max_length=30)
    prenom=models.CharField(max_length=30)
    contact= models.TextField(blank=True)
    

    def __str__(self):
        return self.nom

class Commande(models.Model):
    produit=models.ForeignKey('Produit',on_delete=models.CASCADE)
    user=models.CharField(max_length=30)

    def __str__(self):
        return self.user