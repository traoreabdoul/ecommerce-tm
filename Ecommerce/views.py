from django.shortcuts import render,redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm
from .forms import CreateUserForm
from .models import Produit,Genre,Categorie,Livreur,Commande
from random import choice
import datetime
from django.contrib.auth.models import User



# page d'acueil
def index(request):
    return render(request,'ecommerce/index.html')

#####################################################################

## fonction d'inscription
def registerPage(request):
    if request.user.is_authenticated:
        return redirect('/') 
    else:
        form = CreateUserForm()
        if request.method == 'POST':
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
                request.session['username'] = user
                messages.success(request, 'Compte cree par: ' + user)
                return redirect('/login')
        context = {'form':form}
        return render(request, 'ecommerce/inscription.html', context)

###################################################################################
#fonction d'authentification
def loginPage(request):
    if request.user.is_authenticated:
        return redirect('/')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password =request.POST.get('password')
            user = authenticate(request, username=username, password=password)
            request.session['username'] = username
            if user is not None:
                login(request, user)
                return redirect('/dashboard')
            else:
                messages.info(request, 'Username OR password is incorrect')

        context = {}
        return render(request, 'ecommerce/login.html', context)


#############################################################################

# accueil dashboard

def dashboard(request):
    return render(request,'ecommerce/dashboard/dashboard.html')

##############################################################################    
# page des chaussure
def chaussure(request):
    talon=Genre.objects.get(nom="Talons")
    basket=Genre.objects.get(nom="Baskets")
    espadrille=Genre.objects.get(nom="Espadrilles")
    talons=Produit.objects.filter(genre=talon)
    baskets=Produit.objects.filter(genre=basket)
    espadrilles=Produit.objects.filter(genre=espadrille)
    return render(request,'ecommerce/dashboard/chaussure.html',{'talons':talons, 'baskets':baskets,'espadrilles':espadrilles,})
############################################################################################

##############################################################################    
# page des accesoires
def accessoire(request):
    montre=Genre.objects.get(nom="Montres")
    chaine=Genre.objects.get(nom="Chaines")
    bracelet=Genre.objects.get(nom="Bracelets")
    montres=Produit.objects.filter(genre=montre)
    chaines=Produit.objects.filter(genre=chaine)
    bracelets=Produit.objects.filter(genre=bracelet)
    return render(request,'ecommerce/dashboard/accessoire.html',{'montres':montres, 'chaines':chaines,'bracelets':bracelets})
############################################################################################


##############################################################################    
# page des sac
def sac(request):
    sac_a_main=Genre.objects.get(nom="Sac_a_main")
    sac_a_do=Genre.objects.get(nom="Sac_a_dos")
    sac_a_dos=Produit.objects.filter(genre=sac_a_do)
    sac_a_mains=Produit.objects.filter(genre=sac_a_main)
    return render(request,'ecommerce/dashboard/sac.html',{'sac_a_dos':sac_a_dos, 'sac_a_mains':sac_a_mains,})
############################################################################################

##############################################################################    
# page des echarpes
def echarpe(request):
    echarpe=Genre.objects.get(nom="Echarpes")
    echarpes=Produit.objects.filter(genre=echarpe)
    return render(request,'ecommerce/dashboard/echarpe.html',{'echarpes':echarpes})
############################################################################################

## Page dee choix du lieu de livraison

def lieuLivraison(request,produit):
    if request.method == 'POST':
        lieu = request.POST.get('lieu')
        request.session['produit'] = produit
        return redirect('/livraison/'+lieu)
    return render(request,'ecommerce/dashboard/lieu.html')


###########################################################################################
###page d'information de livraison

def livraison(request,lieu):
    livreur = choice(Livreur.objects.all())
    today = datetime.date.today()
    one_day = datetime.timedelta(days=1)
    date = today + one_day
    produit=request.session.get('produit')
    produits=Produit.objects.get(nom=produit)
    user=request.session.get('username')
    commande=Commande.objects.create(user=user,produit=produits)
    commande.save()
    return render(request,'ecommerce/dashboard/livraison.html',{'lieu':lieu,'date':date,'livreur':livreur,'produit':produit,'user':user})
    
    
    
def commande(request):
    user=request.session.get('username')
    commande=Commande.objects.filter(user=user)
    return render(request,'ecommerce/dashboard/commande.html',{'commandes':commande})

