# Generated by Django 3.0.7 on 2021-02-08 19:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Ecommerce', '0003_auto_20210208_1925'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='produit',
            name='categorie',
        ),
        migrations.CreateModel(
            name='Type',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=30)),
                ('categorie', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Ecommerce.Categorie')),
            ],
        ),
        migrations.AddField(
            model_name='produit',
            name='types',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='Ecommerce.Type'),
            preserve_default=False,
        ),
    ]
