
from django.contrib import admin
from django.urls import path
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
   path('',views.index,),
   path('register/',views.registerPage),
   path('login/',views.loginPage),
   path('dashboard/',views.dashboard),
   path('chaussure/',views.chaussure),
   path('accessoire/',views.accessoire),
   path('sac/',views.sac),
   path('echarpe/',views.echarpe),
   path('lieu/<str:produit>',views.lieuLivraison),
   path('livraison/<str:lieu>',views.livraison),
   path('commande/',views.commande),
]
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)